optionsData = {
	debugPanel: false,
	logLevel: 'info',	//debug, info, error
	logLineNumbers: true,
	debuggerWhenAssertFailed: true,
	rotateShapesLikeInPervologo: true,
	textboxView: 'quill', //'textarea' or 'quill'
	quillShowToolbar: false,
}